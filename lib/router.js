Router.setTemplateNameConverter(function (str) {
    var component;
    str = Iron.utils.classCase(str);
    if ((Meteor.isClient && Platform.isWeb() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'web') {
        if (Template["web_"+str]) {
          return "web_"+str;
        }
    }
    if ((Meteor.isCordova && Platform.isIOS() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'ios') {
        if (Template["ios_"+str]) {
          return "ios_"+str;
        }
    }
    if ((Meteor.isCordova && Platform.isAndroid() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'android') {
        if (Template["android_"+str]) {
          return "android_"+str;
        }
    }
    return str;
});

ApplicationController = RouteController.extend({
    layoutTemplate: 'ApplicationLayout',
    onStop: function(){
      // register the previous route location in a session variable
      Session.set("previousLocationPath", Router.current().route.path());
    }
});

Router.configure({
    controller: "ApplicationController",
    notFoundTemplate: "NotFound",
    loadingTemplate: "Loading",
    waitOn: function() {
      return Meteor.subscribe('services');
    }
});

AccountsTemplates.configure({
    defaultLayout: 'AccountsLayout',
    enablePasswordChange: false,
    sendVerificationEmail: true,
    showResendVerificationEmailLink: false,
    showForgotPasswordLink: true,
    overrideLoginErrors: false,
    continuousValidation: true,
    showValidating: true,
    negativeFeedback: true,
    showLabels: false,
    //showReCaptcha: true,
});

AccountsTemplates.addField({
  _id: 'terms',
  type: 'checkbox',
  template: "termsCheckbox",
  errStr: "You must agree to the Terms and Conditions",
  func: function(value) {
    return !value;
  },
  negativeValidation: false
});

AccountsTemplates.addField({
  _id: "updates",
  type: "checkbox",
  template: "updatesCheckbox",
  displayName: "I want to stay updated about last news",
});

/*
 * All templates helpers which we can use
 * Here https://github.com/meteor/meteor/tree/master/packages/accounts-ui-unstyled
 * https://github.com/meteor-useraccounts/core/blob/master/Guide.md
 */
/*
change password changePwd       atChangePwd     /change-password        fullPageAtForm
enroll account  enrollAccount   atEnrollAccount /enroll-account fullPageAtForm  X
 */
AccountsTemplates.configureRoute('signIn', {
    name: 'SignIn',
    path: '/login',
    redirect: '/profile',
    template: 'SignIn',
});

AccountsTemplates.configureRoute('signUp', {
    name: 'SignUp',
    path: '/register',
    redirect: '/',
    template: 'SignUp',
});

AccountsTemplates.configureRoute('forgotPwd', {
    name: 'ForgotPwd',
    path: '/forgot',
    redirect: '/login',
    template: 'ForgotPwd',
});

AccountsTemplates.configureRoute('verifyEmail', {
    name: 'VerifyEmail',
    path: '/verify-email',
    redirect: '/profile',
    template: 'VerifyEmail',
});

AccountsTemplates.configureRoute('resetPwd', {
    name: 'ResetPwd',
    path: '/reset-password',
    redirect: '/profile',
    template: 'ResetPwd',
});

AccountsTemplates.configure({
    onLogoutHook: function() {
        //redirect after logout
        Router.go('/login');
    }
});

/*
Router.plugin('ensureSignedIn', {
    except: _.pluck(AccountsTemplates.routes, 'name')
    //.concat(['home', 'contacts'])
});
*/
ClientAreaController = ApplicationController.extend({
    waitOn: function () {
        return [ Meteor.subscribe("roles"),
                 Meteor.subscribe('profile'),
                 Meteor.subscribe("notification"),
                 Meteor.subscribe('services'),
                 Meteor.subscribe('listcurrentrequests')
               ];
    },
    onBeforeAction: function () {
        if (!Roles.userIsInRole(Meteor.userId(), ['client'])) {
            Router.go('/login');
        } else {
          if (Platform.isWeb() && Router.current().route.path() == '/') {
              Router.go('/profile');
              return;
          } else {
              this.next();
          }
        }
    }
});

ApplicationMapController = ClientAreaController.extend({
    subscriptions: function() {
      return [Meteor.subscribe('services')];
    },
    action : function () {
      if (this.ready()) {
        this.render();
      } else {
        this.render('Loading');
      }
    }
});

Router.map(function() {
    this.route('ClientMap', {
        title: 'ClientMap',
        path: '/',
        controller: "ApplicationMapController"
    });
});

Router.map(function() {
    this.route('ClientProfileForm', {
        title: 'Profile',
        path: '/profile',
        controller: 'ClientAreaController',
        data: function () {
            return client_profile.findOne({"id_Client": Meteor.userId()});
        }
    });
});

Router.map(function() {
    this.route('HandymanFeedbacks', {
        title: 'Handyman feedbacks',
        path: '/handyman-feedbacks/:_id',
        controller: 'ClientAreaController',
        data: function () {
            return handyman_profile.findOne({"id_Handyman": this.params._id});
        }
    });
});

Router.map(function() {
    this.route('ListMyChats', {
        title: 'List of my chats',
        path: '/chats',
        controller: 'ClientAreaController',
        data: function () {
          var existing_request_handymen = service_request.find({"status": {"$not": {"$in": ["paid", "closed"]}}}).fetch();

          if(!_.isEmpty(existing_request_handymen)) {
            var handymen_ids = _.map(existing_request_handymen, function(value, index){
              return value.id_Handyman;
            });
            return {"listmychats": chat.find({"id_Handyman": {"$in": handymen_ids}}) };
          }
          return {"listmychats": false};
        },
        waitOn: function() {
          return [Meteor.subscribe('listmychats')];
        }
    });
});

ApplicationChatController = ClientAreaController.extend({
    data: function () {
      var chat_id = chat.findOne({"id_Handyman": this.params._id, "id_Client": Meteor.userId()});
      var _handyman_profile = handyman_profile.findOne({"id_Handyman": this.params._id});
      if(!_.isUndefined(chat_id)) {
        return {"chat_id": chat_id,
                "messages": chat_messages.find({"id_Chat": chat_id._id}, {"sort": {"date" : 1}})};
      }else {
        return {};
      }
    },
    waitOn: function() {
      return [Meteor.subscribe('chat', this.params._id)];
    }
});

Router.map(function() {
    this.route('Chat', {
        title: 'Chat',
        path: '/chat/:_id',
        controller: 'ApplicationChatController'
    });
});

Router.map(function() {
    this.route('ListMyRequests', {
        title: 'My requests',
        path: '/requests',
        controller: 'ClientAreaController',
        data: function () {
          return {"listmyrequests": service_request.find()};
        },
        waitOn: function() {
          return [Meteor.subscribe('listmyrequests')];
        }
    });
});

/*
Router.map(function() {
    this.route('Request', {
        title: 'Request',
        path: '/request/:_id',
        controller: 'ClientAreaController',
        data: function () {
            return service_request.findOne({"_id": this.params._id});
        },
        waitOn: function() {
          return Meteor.subscribe('request', this.params._id);
        }
    });
});
*/

Router.map(function() {
    this.route('ListMyInvoices', {
        title: 'My invoices',
        path: '/invoices',
        controller: 'ClientAreaController',
        data: function () {
          return {"listmyinvoices": service_request.find().fetch()};
        },
        waitOn: function() {
          return Meteor.subscribe('listmyinvoices');
        }
    });
});

Router.map(function() {
    this.route('Support', {
        title: 'Support',
        path: '/support',
        controller: 'ClientAreaController',
        data: function () {
            return handyman_profile.findOne({"id_Handyman": Meteor.userId()});
        }
    });
});

Router.map(function() {
    this.route('About', {
        title: 'About service',
        path: '/about',
        controller: 'ClientAreaController',
        data: function() {
          return {about: nodes.findOne({"type": "about"}), faqlist: nodes.find({"type": "faq"})};
        },
        subscriptions: function() {
          return [Meteor.subscribe('about'), Meteor.subscribe('faq')];
        },
        action : function () {
          if (this.ready()) {
            this.render();
          } else {
            this.render('Loading');
          }
        },
    });
});

Router.map(function() {
    this.route('Terms', {
        title: 'Terms of service',
        path: '/terms',
        data: function() {
          return nodes.findOne({"type": "terms"});
        },
        subscriptions: function() {
          return [Meteor.subscribe('terms')];
        },
        action : function () {
          if (this.ready()) {
            this.render();
          } else {
            this.render('Loading');
          }
        },
        layoutTemplate: 'AccountsLayout'
    });
});
