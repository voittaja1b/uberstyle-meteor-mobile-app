// code to run on server at startup
Meteor.startup(function () {

    if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
        Iron.utils.debug = true;
    }

    if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
        Push.debug = true;
    }

    if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
        Push.debug = true;
    }

    /**
     ** Settings for Logger
     */
    Log = new Logger();

    if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
      new LoggerConsole(Log, {}).enable({
        enable: true,
        filter: ['*'], /* Filters: 'ERROR', 'FATAL', 'WARN', 'DEBUG', 'INFO', 'TRACE', '*' */
        client: true, /* This allows to call, but not execute on Client */
        server: true   /* Calls from client will be executed on Server */
      });
    }

    new LoggerMongo(Log, {}).enable({
      enable: true,
      filter: ['ERROR', 'FATAL', 'WARN'], /* Filters: 'ERROR', 'FATAL', 'WARN', 'DEBUG', 'INFO', 'TRACE', '*' */
      client: false, /* This allows to call, but not execute on Client */
      server: true   /* Calls from client will be executed on Server */
    });

    new LoggerRollbar(Log, {}).enable({
      enable: true,
      filter: ['*'], /* Filters: 'ERROR', 'FATAL', 'WARN', 'DEBUG', 'INFO', 'TRACE', '*' */
      client: true, /* This allows to call, but not execute on Client */
      server: true   /* Calls from client will be executed on Server */
    });

    if (Meteor.isClient) {
        if(typeof(Meteor.settings.public.platformOverride) != "undefined" && Meteor.settings.public.platformOverride) {
            Session.set("platformOverride", Meteor.settings.public.platformOverride);
        }

        if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
            AutoForm.debug();
        }

        if(typeof(Meteor.settings.public.stripe_publishable) != "undefined" && Meteor.settings.public.stripe_publishable) {
          Stripe.setPublishableKey(Meteor.settings.public.stripe_publishable);
        }
    }

    //Section for running only on server
    if (Meteor.isServer) {
      if (Meteor.users.find().count() === 0) {
        Roles.createRole('admin');
        Roles.createRole('client');
        Roles.createRole('handyman');
        Roles.createRole('staff');

        // If nobody in system yet, then add admin user
        if (Meteor.settings.private.user_admin) {
          //Get settings from file of environment variables
          var user = Meteor.settings.private.user_admin;
          var id = Accounts.createUser({
            email: user.email,
            password: user.password,
            profile: { name: user.name }
          });

          Roles.addUsersToRoles(id, user.roles, Roles.GLOBAL_GROUP);
        }
      }

      // set the settings object with meteor --settings settings-development.json
      if(typeof(Meteor.settings.private.facebook) != "undefined" && Meteor.settings.private.facebook) {
          ServiceConfiguration.configurations.remove({
              service: "facebook"
          });

          ServiceConfiguration.configurations.insert({
              service: "facebook",
              appId: Meteor.settings.private.facebook.clientId,
              secret: Meteor.settings.private.facebook.secret
          });
      }
      if(typeof(Meteor.settings.private.twitter) != "undefined" && Meteor.settings.private.twitter) {
          ServiceConfiguration.configurations.remove({
              service: "twitter"
          });

          ServiceConfiguration.configurations.insert({
              service: "twitter",
              appId: Meteor.settings.private.twitter.clientId,
              secret: Meteor.settings.private.twitter.secret
          });
      }
      if(typeof(Meteor.settings.private.google) != "undefined" && Meteor.settings.private.google) {
          ServiceConfiguration.configurations.remove({
              service: "google"
          });

          ServiceConfiguration.configurations.insert({
              service: "google",
              appId: Meteor.settings.private.google.clientId,
              secret: Meteor.settings.private.google.secret
          });
      }

      Accounts.emailTemplates.siteName = Meteor.settings.public.sitename;
      Accounts.emailTemplates.from = Meteor.settings.private.email.from;

      Mailer.config({
          from: Meteor.settings.private.email.from,     // Default 'From:' address. Required.
          replyTo: Meteor.settings.private.email.replyTo,  // Defaults to `from`.
          routePrefix: 'emails',              // Route prefix.
          baseUrl: process.env.ROOT_URL,      // The base domain to build absolute link URLs from in the emails.
          testEmail: null,                    // Default address to send test emails to.
          logger: Log,                 // Injected logger (see further below)
          silent: false,                      // If set to `true`, any `Logger.info` calls won't be shown in the console to reduce clutter.
          addRoutes: Meteor.settings.public.debug, // Add routes for previewing and sending emails. Defaults to `true` in development.
          language: 'html',                    // The template language to use. Defaults to 'html', but can be anything Meteor SSR supports (like Jade, for instance).
          plainText: true,                     // Send plain text version of HTML email as well.
          plainTextOpts: {}                   // Options for `html-to-text` module. See all here: https://www.npmjs.com/package/html-to-text
      });

      /**
      * Debug of SimpleSchema
      */
      if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
        SimpleSchema.debug = true
      }

      if(typeof(Meteor.settings.private.stripe_secret) != "undefined" && Meteor.settings.private.stripe_secret) {
        Stripe = StripeAPI(Meteor.settings.private.stripe_secret);
      }

    }
});
