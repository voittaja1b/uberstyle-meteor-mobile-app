### Pull refresh plugin for Meteor

Package is based on repo [click here](https://github.com/misak113/bootstrap.pull-down.js)

# How to use it?

## Add package

```
meteor add logvik:bootstrap-pull-refresh
```

# Documentation

Documentation: [click here](https://github.com/misak113/bootstrap.pull-down.js)