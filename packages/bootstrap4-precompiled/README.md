### Compiled Bootstrap 4 Alpha for Meteor

Current version: Bootstrap v4.0.0-alpha.2, based on repo [click here](https://github.com/dmxt/bootstrap-4-alpha-compiled)

# How to use it?

## Add package

```
meteor add logvik:bootstrap4-precompiled
```

## Load Bootstrap4 styles

Add meta and link tags to head.

```html
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">

  <!-- Your app title -->
  <title>Application Title</title>

  <!-- Bootstrap4 theme styles -->
  <link rel="stylesheet" href="/packages/logvik_bootstrap4-precompiled/bootstrap4/css/bootstrap.min.css">
</head>
```

# Documentation

Bootstrap v4 documentation: [click here](http://v4-alpha.getbootstrap.com/getting-started/introduction/)