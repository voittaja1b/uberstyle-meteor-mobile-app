Package.describe({
  name: 'logvik:loggerrollbar',
  version: '1.0.1',
  summary: 'Adapter Rollbar for ostrio logger',
  git: 'https://github.com/logvik/loggerrollbar.git',
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.0');
  api.use(["saucecode:rollbar", "ostrio:logger@1.1.0"]);
  api.addFiles('rollbaradapter.js');
  api.export('LoggerRollbar');
});
