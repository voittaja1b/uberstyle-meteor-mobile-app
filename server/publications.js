/**
* Publisher function will send doses of information to client
*/

Meteor.publish("roles", function (){
  return Meteor.roles.find({});
});
Meteor.publish("notification", function (){
  return notification_history.find({"id_User": this.userId});
});
Meteor.publish('profile', function() {
  return client_profile.find({"id_Client": this.userId}, {"fields": {"stripeCustomer": 0, "stripeToken": 0}});
});
Meteor.publish('handymen', function(location) {
  //Calculate a time in annotation of cycle minutes in 24 period
  //var time = new Date().getHours() * 60 + new Date().getMinutes();
  var params = {};
  //{"availability.from": { $lte: time }, "availability.to": { $gte: time }};
  if(location) {
    //here we must use algorithm for definition of circle area around of user location ~ 40km
    params["currentPosition"] = { $geoWithin: { $centerSphere: [ [location.lat, location.lng] , (40/6378.1) ] } };
    return handyman_profile.find(params);
  } else {
    return handyman_profile.find({_id: 1});
  }
});

Meteor.publish('services', function() {
  return services.find();
});

Meteor.publishTransformed('listmychats', function(limit) {
  var _limit = limit || 50;
  return chat.find({"id_Client": this.userId}, {"limit": _limit, "sort": {"dateLastMessage": -1}}).serverTransform({
    // we extending the document with the custom property 'handyman_profile'
    handyman_profile: function(doc) {
      return handyman_profile.findOne({
        "id_Handyman": doc.id_Handyman
      });
    }
  });
});

Meteor.publish('chat', function(id_Handyman, limit) {
  var id_Client = this.userId;
  var _limit = limit || 50;
  var _chat = chat.find({"id_Handyman": id_Handyman, "id_Client": id_Client});
  if(_chat.fetch().length == 0) {
    chat.insert({"id_Handyman": id_Handyman, "id_Client": id_Client, "dateLastMessage": new Date()});
    _chat = chat.find({"id_Handyman": id_Handyman, "id_Client": id_Client});
  }
  _handyman_profile = handyman_profile.find({"id_Handyman": id_Handyman});
  return [_chat, _handyman_profile, chat_messages.find({"id_Chat": _chat.fetch()[0]._id}, {"limit" : _limit, "sort": {"date": -1}})];
});


Meteor.publish('chatHistory', function(_id_Chat, limit) {
  var _limit = parseInt(limit) || 50;
  return chat_messages.find({"id_Chat": _id_Chat}, {"limit" : _limit, "sort": {"date": -1}});
});

Meteor.publishTransformed('listmyrequests', function(limit) {
  var _limit = limit || 50;
  return service_request.find({"id_Client": this.userId}, {"limit": _limit, "sort": {"dateServicing": -1}}).serverTransform({
    // we extending the document with the custom property 'handyman_profile'
    handyman_profile: function(doc) {
      return handyman_profile.findOne({
        "id_Handyman": doc.id_Handyman
      });
    },
    service: function (doc) {
      return services.findOne({
        "_id": doc.id_Service
      });
    },
    invoice: function (doc) {
      return invoice.findOne({
        "id_ServiceRequest": doc._id
      });
    }
  });
});

//'paid', 'closed'
Meteor.publishTransformed('listcurrentrequests', function(limit) {
  var _limit = limit || 50;
  return service_request.find({"id_Client": this.userId, "status": {"$not": {"$in": ["paid", "closed"]}}}, {"limit": _limit, "sort": {"dateServicing": -1}}).serverTransform({
    // we extending the document with the custom property 'handyman_profile'
    handyman_profile: function(doc) {
      return handyman_profile.findOne({
        "id_Handyman": doc.id_Handyman
      });
    },
    service: function (doc) {
      return services.findOne({
        "_id": doc.id_Service
      });
    },
    invoice: function (doc) {
      return invoice.findOne({
        "id_ServiceRequest": doc._id
      });
    }
  });
});

Meteor.publishTransformed('listmyinvoices', function(limit) {
  var _limit = limit || 50;
  return service_request.find({"id_Client": this.userId, "status": "approved"}, {"limit": _limit, "sort": {"dateServicing": -1}}).serverTransform({
    // we extending the document with the custom property 'handyman_profile'
    handyman_profile: function(doc) {
      return handyman_profile.findOne({
        "id_Handyman": doc.id_Handyman
      });
    },
    service: function (doc) {
      return services.findOne({
        "_id": doc.id_Service
      });
    },
    invoice: function (doc) {
      return invoice.findOne({
        "id_ServiceRequest": doc._id
      });
    }
  });
});

Meteor.publish('request', function(id) {
  return service_request.find({"_id": id, "id_Client": this.userId});
});

Meteor.publish('terms', function() {
  return nodes.find({"type": "terms"});
});

Meteor.publish('about', function() {
  return nodes.find({"type": "about"});
});

Meteor.publish('faq', function() {
  return nodes.find({"type": "faq"});
});
