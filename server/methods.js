/**
 *
 */

var clientProfileStore = function (doc) {
  check(doc.insertDoc, schemas.client_profile);
  if(!doc._id) {
    if(doc.insertDoc.password) {
      Accounts.setPassword(Meteor.userId(), doc.insertDoc.password);
    }
    var emailAlreadyExist = Meteor.users.find({"emails.$.address": doc.insertDoc.email}, {limit: 1}).count() > 0;
    if(!emailAlreadyExist) {
      Meteor.users.update({_id: Meteor.userId()}, {$set:{"emails.0.address": doc.insertDoc.email}});
    }else {
      Meteor.Error("Email already exists. You must use other email.");
    }
    return client_profile.insert(doc.insertDoc);
  } else {
    client_profile.update({_id: doc._id}, doc.updateDoc);
    if(doc.insertDoc.email != Meteor.user().emails[0].address) {
      var emailAlreadyExist = Meteor.users.find({"emails.address": doc.insertDoc.email}, {limit: 1}).count() > 0;
      if(!emailAlreadyExist) {
        Meteor.users.update({_id: Meteor.userId()}, {$set:{"emails.0.address": doc.insertDoc.email}});
      }else {
        Log.error("Email already exists Data context: " + EJSON.stringify(doc) + " User context: " + EJSON.stringify(Meteor.user()));
        throw new Meteor.Error(501, "Email already exists. You must use other email.");
      }
    }
    if(doc.insertDoc.password) {
      Accounts.setPassword(Meteor.userId(), doc.insertDoc.password, {"logout": false});
    }
    return true;
  }
}

Meteor.methods({
  sendClientProfile: function(doc) {
    doc.insertDoc.id_Client = Meteor.userId();
    doc.updateDoc.$set.id_Client = Meteor.userId();

    //@DEBUG
    Log.debug(EJSON.stringify(doc));
    //

    if(!_.isEmpty(doc.insertDoc.stripeToken)) {

      // A checking of case when we have already a customer and just must update him
      var current_stripeCustomer = client_profile.find({"id_Client": Meteor.userId()},{"fields" : {"stripeCustomer": 1}}).fetch();
      console.log(current_stripeCustomer);
      if(!_.isUndefined(current_stripeCustomer) &&
         !_.isUndefined(current_stripeCustomer[0].stripeCustomer)) {
        Stripe.customers.update(current_stripeCustomer[0].stripeCustomer, {
          source: doc.insertDoc.stripeToken // obtained with Stripe.js
        }).then(Meteor.bindEnvironment(function(customer){
          doc.insertDoc.stripeCustomer = customer.id;
          doc.updateDoc.$set.stripeCustomer = customer.id;
          doc.insertDoc.certified = true;
          doc.updateDoc.$set.certified = true;
          return clientProfileStore(doc);
        })).catch(Meteor.bindEnvironment(function(error){
          Log.error(EJSON.stringify(error));
          throw new Meteor.Error(error.message);
        }));
      } else {
        Stripe.customers.create({
          email: doc.insertDoc.email,
          source: doc.insertDoc.stripeToken // obtained with Stripe.js
        }).then(Meteor.bindEnvironment(function(customer){
          doc.insertDoc.stripeCustomer = customer.id;
          doc.updateDoc.$set.stripeCustomer = customer.id;
          doc.insertDoc.certified = true;
          doc.updateDoc.$set.certified = true;
          return clientProfileStore(doc);
        })).catch(Meteor.bindEnvironment(function(error){
          Log.error(EJSON.stringify(error));
          throw new Meteor.Error(error.message);
        }));
      }
    } else {
      return clientProfileStore(doc);
    }
  },
  locationByAddress:function(address){
    var geo = new GeoCoder();
    var result = geo.geocode(address);
    return result;
  },
  addressByLocation: function(location){
    var geo = new GeoCoder();
    var result = geo.reverse(location.lat, location.lng);
    return result;
  },
  placeRequest: function(request) {
    request.id_Client = Meteor.userId();
    request.status = 'estimation';

    //@DEBUG
    Log.debug(EJSON.stringify(request));
    check(request, schemas.service_request);

    try{
      service_request.insert(request);
      serviceName = services.findOne({_id: request.id_Service}).name;

      sendNotification("Request", "New request", "The request by address \"" + request.destinationAddress + "\" for \"" + serviceName + "\"", request.id_Handyman, "ListRequests");
      return true;
    } catch(e) {
      throw Log.error(e);
    }
  },
  sendMessageToChat: function(doc) {
    doc.from = Meteor.userId();
    doc.date = new Date();
    //@DEBUG
    Log.debug(EJSON.stringify(doc));
    //
    check(doc, schemas.chat_messages);
    if(!doc._id) {
      var last_id = chat_messages.insert(doc);
      if(last_id) {
        chat.update({_id: doc.id_Chat}, {$inc: {"countMessages": 1}, $set: {"dateLastMessage": new Date()}});
      }
      var to_handyman = chat.findOne({_id: doc.id_Chat}, {"fields": {id_Handyman:1, id_Client:1}});

      sendNotification("Chat", "New message", doc.message, to_handyman.id_Handyman, "Chat", to_handyman.id_Client);

      return last_id;
    } else {
      chat_messages.update({_id: doc._id}, doc);
      return true;
    }
  },
  sendSupport: function(doc) {
    // Important server-side check for security and data integrity

    doc = doc.insertDoc;

    //@DEBUG
    Log.debug(EJSON.stringify(doc));
    //

    check(doc, schemas.contact);

    // Build the e-mail text
    var text = "Name: " + doc.name + "\n\n"
            + "Email: " + doc.email + "\n\n\n\n"
            + doc.message;

    // Send the e-mail
    Email.send({
        to: Meteor.settings.private.user_admin.email,
        from: doc.email,
        subject: "Website Contact Form - Message From " + doc.name,
        text: text
    });

    return true;
  },
  changeStatus: function(id, status) {
    var _id_Handyman = service_request.findOne({_id: id}, {"fields": {id_Handyman: 1, destinationAddress: 1}});

    return service_request.update({_id: id}, {$set: {status: status}}, function(error, result) {
      if(!error) {
        if(status == "approved") {
          invoice.update({"id_ServiceRequest": id}, {$set: {status: "unpaid"}}, function(error, result) {
            if(!error) {
              sendNotification("Status", "Status has been changed", "The request by address \"" + _id_Handyman.destinationAddress + "\" now is \"" + status + "\"", _id_Handyman.id_Handyman, "ListRequests");
            } else {
              throw new Meteor.Error(error);
            }
          });
        } else {
          sendNotification("Status", "Status has been changed", "The request by address \"" + _id_Handyman.destinationAddress + "\" now is \"" + status + "\"", _id_Handyman.id_Handyman, "ListRequests");
        }
      } else {
        throw new Meteor.Error(error);
      }
    });
  },
  leaveFeedback: function(feedback) {
    check(feedback, schemas.feedbacks);

    var obj = {
      "$set": feedback
    };

    //@DEBUG
    Log.debug(EJSON.stringify(obj));
    //

    if(feedbacks.upsert({"id_ServiceRequest": feedback.id_ServiceRequest}, obj)) {
      var id_Handyman = service_request.findOne({"_id": feedback.id_ServiceRequest}, {"fields": {"id_Handyman": 1}});
      var rating = handyman_profile.findOne({"id_Handyman": id_Handyman.id_Handyman}, {"fields": {"rating": 1}});

      service_request.update({_id: feedback.id_ServiceRequest}, {$set: {status: "closed"}});
      return handyman_profile.update({"id_Handyman": id_Handyman.id_Handyman},
                              {$inc:{countFeedbacks: 1},
                               $set: {"rating": (parseInt(rating.rating)+parseInt(feedback.rating))/2}
                              });
    }
  },
  deleteNotification: function(id) {
    notification_history.remove({_id: id});
  },
  updateClientLocation: function(location) {
    if(!location && Meteor.user()) {
      return;
    }
    var profile_exists = client_profile.findOne({"id_Client": Meteor.userId()});
    if(!profile_exists) {
      var doc = {"id_Client": Meteor.userId(),
                 "email": Meteor.user().emails[0]['address'],
                 "currentPosition": {"lat": location.lat, "lng": location.lng}};
      client_profile.insert(doc);
    } else {
      client_profile.update({_id: profile_exists._id}, {$set:{"currentPosition.lat": location.lat, "currentPosition.lng": location.lng}});
    }
  },
  payInvoice: function(id) {
    var _invoice = invoice.find(id).fetch();
    if(_.isEmpty(_invoice)) {
      throw new Meteor.Error("Invoice not found");
    }
    var _service_request = service_request.find(_invoice[0].id_ServiceRequest).fetch();
    if(_.isEmpty(_service_request)) {
      throw new Meteor.Error("Request related with invoice not found");
    }
    var _client_profile = client_profile.find({"id_Client": _service_request[0].id_Client}).fetch();
    if(_.isEmpty(_client_profile)) {
      throw new Meteor.Error("Client profile related with invoice not found");
    }
    if(_.isEmpty(_client_profile[0].stripeCustomer)) {
      throw new Meteor.Error("Payment information for client profile is invalid");
    }

    var charge = Meteor.wrapAsync(Stripe.charges.create, Stripe.charges);
    var charge_amount = Math.round(_invoice[0].total * 100);
    return charge({
      amount: charge_amount, //always send cents
      currency: 'usd',
      customer: _client_profile[0].stripeCustomer
    }, function(error, charge) {
      if (error) {
        Log.error(EJSON.stringify(error));
        throw new Meteor.Error(error.message);
      }

      invoice.update(id, {$set: {status: "paid"}}, function(error, result) {
        if(error) {
          throw new Meteor.Error(error);
        }
      });

      service_request.update(_invoice[0].id_ServiceRequest, {$set: {status: "paid"}}, function(error, result) {
        if(error) {
          throw new Meteor.Error(error);
        }
      });

      var doc = {id_Invoice: _invoice[0]._id, transactionId: charge.id, sum: charge_amount/100, message: EJSON.stringify(charge)};
      payments.insert(doc, function(error, result) {
        if(error) {
          throw new Meteor.Error(error);
        }
      });

      sendNotification("Payment", "Request have been paid", "You have done a request and got a payment on " + charge_amount + "$", _service_request[0].id_Handyman);

      Log.debug(EJSON.stringify(charge));
      return true;
    });
  }
});

var sendNotification = function(from, title, message, userId, path, pathId) {
  var pathId = pathId || "";
  var _client_profile = client_profile.findOne({id_Client: Meteor.userId()}, {"fields": {fullName: 1}});

  var doc = { id_User: userId, message: message, from: _client_profile.fullName, path: path, pathId: pathId};
  notification_history.insert(doc, function (error, result) {
			if (!error) {
        Push.send({
          from: from,
          title: title,
          text: message,
          //badge: 12,
          sound: "/audio/alert.mp3",
          query: {
            userId: userId
          }
        });
			}
	});
};
