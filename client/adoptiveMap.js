/**
* Some patch for Adoptive map
* @TODO add pull request to author
*/

/*
	For example
	marker = {
	  _id: "my location",
	  transform: function(marker) {
	    return marker;
	  },
	  onClick: function (event) {
	    console.log(event);
	  },
	  position: {lat: 59.327, lng: 18.067},
	  icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
	  // This marker is 20 pixels wide by 32 pixels high.
	  size: new google.maps.Size(20, 32),
	  // The origin for this image is (0, 0).
	  origin: new google.maps.Point(0, 0),
	  // The anchor for this image is the base of the flagpole at (0, 32).
	  anchor: new google.maps.Point(0, 32),
	  infoWindowAnchor: [0,0]
	}
*/
AdaptiveMaps.Map.prototype.addMarker = function (marker) {
	var self = this,
	    transform = marker.transform || function (marker) { return marker;},
	    onClick = marker.onClick || function (event) { },
			onDragStart = marker.onDragStart || function (event) { },
			onDrag = marker.onDrag || function (event) { },
			onDragEnd = marker.onDragEnd || function (event) { },
			autoRemove = marker.autoRemove || true,
			onAutoRemove = marker.onAutoRemove || function (id) { };

  var _id = marker._id,
      marker = AdaptiveMaps._transformMarker(marker, transform);

	if(autoRemove && self._markers[_id]) {
		if (Meteor.isCordova) {
			self._markers[_id].remove();
		} else {
			self._markers[_id].setMap(null);
		}
		onAutoRemove(_id);
	}

  if (Meteor.isCordova) {
  	self.originalMap.addMarker(marker, function (_marker) {
  		_marker.setIcon({
  			url: marker.icon,
  			size: marker.size
  		});
  		_marker.setInfoWindowAnchor(marker.infoWindowAnchor[0], marker.infoWindowAnchor[1]);
  		_marker.setIconAnchor(marker.anchor.x, marker.anchor.y);
  		_marker.addEventListener(plugin.google.maps.event.MARKER_DRAG_START, onDragStart);
			_marker.addEventListener(plugin.google.maps.event.MARKER_DRAG, onDrag);
			_marker.addEventListener(plugin.google.maps.event.MARGER_DRAG_END, onDragEnd);
  		self._markers[_id] = _marker;
  	});
  } else {
  	_.extend(marker, { map: self.originalMap });
		//make it for retina
		if(typeof(marker.icon) != "object") {
			marker.icon = new google.maps.MarkerImage(marker.icon, null, null, null, marker.size);
		}
  	var _marker = new google.maps.Marker(marker);
  	google.maps.event.addListener(_marker, 'click', onClick);
		//Dragable on Cordova https://github.com/mapsplugin/cordova-plugin-googlemaps/wiki/Marker
		google.maps.event.addListener(_marker, 'dragstart', onDragStart);
		google.maps.event.addListener(_marker, 'drag', onDrag);
		google.maps.event.addListener(_marker, 'dragend', onDragEnd);
  	self._markers[_id] = _marker;
  }
};

AdaptiveMaps.Map.prototype.changeMarker = function (marker) {
	var self = this,
			_id = marker._id,
			transform = marker.transform || function (marker) { return marker;},
	    marker = AdaptiveMaps._transformMarker(marker, transform),
	    _markers = self._markers[_id];

	if(!_markers) {
		return;
	}

	if (Meteor.isCordova) {
		_.each(marker, function (value, key) {
			var setKey;

			// special cases
			if (key === 'icon') {
				_markers.setIcon({ url: value, size: marker.size });
			} else if (key === 'infoWindowAnchor') {
				_markers.setInfoWindowAnchor(value[0], value[1]);
			} else if (key === 'anchor') {
				_markers.setIconAnchor(marker.anchor.x, marker.anchor.y);

			// most cases
			} else {
				// modify with method if possible
				setKey = 'set' + key.charAt(0).toUpperCase() + key.substring(1);
				if (typeof _markers[setKey] === 'function')
					_markers[setKey](value);
				else
					_markers.set(key, value);
			}
		});
	} else {
		_.extend(marker, { map: self.originalMap });
		_markers.setOptions(marker);
	}
};

AdaptiveMaps.Map.prototype.removeMarker = function (marker) {
	var self = this,
			_marker = self._markers[marker._id];
	if(!_marker) {
		return;
	}
	if (Meteor.isCordova) {
		_marker.remove();
	} else {
		_marker.setMap(null);
	}
	delete self._markers[marker._id];
};

/**
 * Zooming the map.
 * @function module:AdaptiveMaps.Map#setZoom
 * @param {Number} zoom - zoom of map
 */
AdaptiveMaps.Map.prototype.setZoom = function (zoom) {
	var self = this;
	self.originalMap.setZoom(zoom);
}

/**
 * Get center of map.
 * @function module:AdaptiveMaps.Map#getCenter
 */
AdaptiveMaps.Map.prototype.getCenter = function () {
	var self = this;
	return self.originalMap.getCenter();
}

/**
 * Add listener on map.
 * @param {String} event
 * @function on event
 */
AdaptiveMaps.Map.prototype.addEventListener = function(event, onEvent) {
	var self = this;
	switch (event) {
		case "center_changed":
			if (Meteor.isCordova) {
				self.originalMap.on(plugin.google.maps.event.CAMERA_CHANGE, onEvent);
			} else {
				self.originalMap.addListener('center_changed', onEvent);
			}
			break;
		default:
			break;
	}
}
