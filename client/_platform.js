/**
 *
 */

 /**
  * @Comment on delete
  * https://github.com/nicooprat/meteor-framework7-routing/blob/master/client/index.js
  */
// Initialize app
F7 = new Framework7({
   router: false,
   swipeBackPage: true,
   animatePages: true
});

Platform = {
  isIOS: function () {
    return (F7.device.os == "ios" && !Session.get('platformOverride'))
           || Session.get("platformOverride") === "ios";
  },

  isAndroid: function () {
    return (F7.device.os ==  "android" && !Session.get('platformOverride'))
           || Session.get('platformOverride') === 'android';
  },

  isWeb: function () {
    return (F7.device.os == undefined && !Session.get('platformOverride'))
           || Session.get('platformOverride') === 'web';
  }
};

/**
 * @TODO include in separate package
 */
Template.extendOnPlatforms = function (dict, _name) {
    _.extend(Template, _.map(dict, function(name) {
        if(Template[name + "_" + _name] && Template[_name]) {
            _.extend(Template[name + "_" + _name].__helpers, Template[_name].__helpers);
            _.extend(Template[name + "_" + _name].__eventMaps, Template[_name].__eventMaps);
        }
    }));
}
