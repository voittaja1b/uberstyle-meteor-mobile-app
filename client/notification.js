bootstrap_alert = function() {};
bootstrap_alert.notification = function(id, message) {
    $('#alert_placeholder').append('<div id="notification_' + id + '" class="alert alert-success alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'+message+'</div>')
    var snd = new Audio('/audio/alert.mp3');
    snd.play();
    delete(snd);
    
    Meteor.setTimeout(function(){
       $('#alert_placeholder').find('#notification_' + id).remove();
       if(id) {
         Meteor.call("deleteNotification", id);
       }
    }, 5000);
};
bootstrap_alert.close = function(id, message) {
    $('#alert_placeholder').find('#notification_' + id).remove();
};
bootstrap_alert.closeAll = function() {
    $('#alert_placeholder').empty();
};

showNotification = function(message, id) {
  var id = id || false;
  if (Meteor.isCordova) {

  } else {
    if(Platform.isWeb() || Session.get('platformOverride') === 'web') {
      bootstrap_alert.notification(id, message);
    }
    if(Platform.isAndroid() || Session.get('platformOverride') === 'android') {
      myApp7.addNotification({
          message: message,
          hold: 5000,
          button: {
              text: 'Close',
              color: 'lightgreen'
          },
          onClose: function () {
            if(id) {
              Meteor.call("deleteNotification", id);
            }
          }
      });
    }
    if(Platform.isIOS() || Session.get('platformOverride') === 'ios') {
      myApp7.addNotification({
          title: message,
          message: 'Click icon to close',
          hold: 5000,
          onClose: function () {
            if(id) {
              Meteor.call("deleteNotification", id);
            }
          }
      });
    }
  }
}
