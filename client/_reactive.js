/**
* Here we just define global Dictionary for all global variables of our App with reactive essence.
*/
globalDict = new ReactiveDict('globalDict');

/*
globalDict.set("weather", "cloudy");
Tracker.autorun(function () { console.log("now " + globalDict.get("weather")); });
// now cloudy
globalDict.set("weather", "sunny");
// now sunny
*/

PlainDict = (function() {
  function PlainDict() {};

  PlainDict.prototype.set = function(name, item) {
    this.name = item;
  };

  PlainDict.prototype.get = function(name) {
    if(!_.isUndefined(this.name)) {
      return this.name;
    }
    return null;
  };
  return PlainDict;
})();

globalPlainDict = new PlainDict();
