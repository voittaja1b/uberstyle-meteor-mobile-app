SimpleSchema.messages({
  "regEx phone": [
    {exp: /^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]{3,25}$/, msg: "Format by +* (***) ***-***-**"}
  ],
  "regEx cardnumber": [
    {exp: /^\d{4} \d{4} \d{4} \d{4}$/, msg: "Format by **** **** **** ****"}
  ],
  "regEx cvc": [
    {exp: /^\d{3}$/, msg: "Format by ***"}
  ],
  "regEx expired": [
    {exp: /^\d{2}\/\d{2}$/, msg: "Format by **/**"}
  ],
  "invalid_number cardnumber": "The card number is not a valid credit card number.",
  "invalid_expiry_month expired": "The card's expiration month is invalid.",
  "invalid_expiry_year expired": "The card's expiration year is invalid.",
  "invalid_cvc cvc": "The card's security code is invalid.",
  "incorrect_number cardnumber": "Your card number is incorrect.",
  "expired_card cardnumber": "The card has expired.",
  "incorrect_cvc cvc": "The card's security code is incorrect.",
  "card_declined cardnumber": "The card was declined.",
  "missing cardnumber": "There is no card on a customer that is being charged.",
  "processing_error cardnumber": "An error occurred while processing the card."
});

Template.registerHelper('isIOS', function () {
  return Platform.isIOS();
});

Template.registerHelper('isAndroid', function () {
  return Platform.isAndroid();
});

Template.registerHelper('isWeb', function () {
  return Platform.isWeb();
});

Template.registerHelper('titleApp', function () {
  return Meteor.settings.public.sitename;
});

Template.registerHelper('platformClasses', function () {
  if ((Meteor.isClient && Platform.isWeb() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'web') {
    return 'platform-web';
  }
  if ((Platform.isIOS() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'ios') {
    return 'platform-ios';
  }
  if ((Platform.isAndroid() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'android') {
    return 'platform-android';
  }
  return 'platform-web';
});

Template.registerHelper('curretnPath', function () {
  return Router.current().route.getName();
});

Template.Chat.helpers({
   puredate: function () {
     return moment(this.date).format("dddd, MMMM Do YYYY");
   },
   puretime: function () {
     return moment(this.date).format("HH:mm");
   },
   myMessage: function () {
     return (Meteor.userId() == this.from ? "sent" : "received");
   },
   author: function () {
     var name = "";
     if(Meteor.userId() == this.from) {
       name = "It is my";
     } else {
       var _handyman_profile = handyman_profile.findOne({"id_Handyman": this.from});
       if(_handyman_profile != null && _handyman_profile.fullName) {
         name = _handyman_profile.fullName
       } else {
         name = "Handyman";
       }
     }
     return name;
   }
});

Template.Chat.onRendered(function () {
  this.find('.messages')._uihooks = {
    insertElement: function (node, next) {
      $(node).insertBefore(next);

      Deps.afterFlush(function() {
        // call width to force the browser to draw it
        //$(node).width();
        var height = 0;
        if(!Session.get("loadingHistory")) {
          height = $(".messages").outerHeight();
        }
        $(".page-content").stop().animate({scrollTop: height}, '500', 'swing');
      });
    }
  }
  if(Router.current().route.getName() === "Chat") {
    myApp7.initPullToRefresh(".page-content");
  }else {
    myApp7.destroyPullToRefresh(".page-content")
  }
  var height = $(".messages").outerHeight();
  $(".page-content").stop().animate({scrollTop: height}, '500', 'swing');
});

Template.ListMyChats.helpers({
   handymanName: function () {
     return (this.handyman_profile != null && this.handyman_profile.fullName ? this.handyman_profile.fullName : "Some handyman");
   }
});

Template.ListMyRequests.helpers({
   handymanName: function () {
     return (this.handyman_profile != null && this.handyman_profile.fullName ? this.handyman_profile.fullName : "Some handyman");
   },
   date: function () {
     return moment(this.dateServicing).format("dddd, MMMM Do YYYY HH:mm");
   },
   formatComment: function () {
     return _.isUndefined(this.comment) ? "No additional comment" : this.comment;
   },
   statusIcon: function () {
     switch(this.status) {
       case "estimation":
         return "fa-money";
         break;
       case "progress":
         return "fa-truck";
         break;
       case "done":
         return "fa-check";
         break;
       case "approved":
         return "fa-check-circle-o";
         break;
      case "closed":
         return "fa-times";
         break;
     }
   },
   selected: function (status) {
     return (this.status == status ? "selected": "");
   },
   disabled: function (status) {
     switch(status) {
       case "estimation":
         return !_.isUndefined(this.invoice) ? "disabled" : "";
         break;
       case "progress":
         return _.isUndefined(this.invoice) ? "disabled" : "";
         break;
       case "done":
         return true ? "disabled" : "";
         break;
       case "approved":
         return _.isUndefined(this.invoice) ? "disabled" : "";
         break;
       case "closed":
         return !_.isUndefined(this.invoice) ? "disabled" : "";
         break;
     }
   },
   availableChat: function() {
     return !_.include(["paid", "closed"], this.status);
   },
   formatStatus : function() {
     $("div[idRequest='" + this._id + "']").text("");
     return this.status.charAt(0).toUpperCase() + this.status.slice(1);
   },
   leaveFeedback: function() {
     return this.status == "approved";
   },
   checkInvoice: function(){
     return !_.isUndefined(this.invoice);
   },
   invoiceServiceCost: function() {
     return !_.isUndefined(this.invoice) && !_.isUndefined(this.invoice.serviceCost) ? this.invoice.serviceCost : false;
   },
   invoiceTransportCost: function() {
     return !_.isUndefined(this.invoice) && !_.isUndefined(this.invoice.transportCost) ? this.invoice.transportCost : false;
   },
   invoiceExtrasCost: function() {
     return !_.isUndefined(this.invoice) && !_.isUndefined(this.invoice.extrasCost) ? this.invoice.extrasCost : false;
   },
   invoiceTax: function() {
     return !_.isUndefined(this.invoice) && !_.isUndefined(this.invoice.tax) ? this.invoice.tax : false;
   },
   invoiceTotal: function() {
     return !_.isUndefined(this.invoice) && !_.isUndefined(this.invoice.total) ? this.invoice.total : false;
   }
});

Template.ListMyRequests.events({
  'change #status': function (e) {
    Meteor.call("changeStatus", this._id, $("#status").val(), function(error, result) {
      if(error) {
        myApp7.alert(error, "Ups, something happened");
        return;
      } else {
        showNotification("Saved");
      }
    });
  },
  "click #leaveFeedback": function() {
    var requestObj = this;
    var rating   = "";
    var feedbackRating = 0;
    _.each(_.range(0, 5), function (i) {
      i++;
      rating += '<i stars="' + i + '" class="fa fa-2 fa-star-o"></i>';
    });
    var modal = myApp7.modal({
      title:  'Leave feedback for Handyman',
      text: '<div class="list-block">'+
                    '<ul>'+
                      '<li class="align-top">'+
                        '<div class="item-title full-label">Estimation of done work</div>'+
                        '<div class="item-input" id="feedbackRating">'+
                          '<p>' + rating + '</p>' +
                        '</div>'+
                      '</li>'+
                      '<li class="align-top">'+
                        '<div class="item-title full-label">Comment</div>'+
                        '<div class="item-input">'+
                          '<textarea id="feedback" placeholder="Describe your relation for done work here."></textarea>'+
                        '</div>'+
                      '</li>'+
                    '</ul>'+
                  '</div>',
      buttons: [
        {
          text: 'Cancel',
          bold: true,
          close: true,
        },
        {
          text: 'Post',
          bold: true,
          onClick: function() {
            if(!feedbackRating) {
              myApp7.alert("You must estimate via stars.", "Ups, something happened");
              return false;
            }
            var feedback = {
              id_ServiceRequest: requestObj._id,
              message: $("#feedback").val(),
              rating: parseInt(feedbackRating)
            };
            Meteor.call("leaveFeedback", feedback, function(error, result){
              if(error){
                console.log("error", error);
                myApp7.alert(error, "Ups, something happened");
              }
              if(result){
                showNotification("You have been left your feedback successfully");
              }
            });
          }
        },
      ]
    });

    $(modal).on("opened", function() {
      $("#feedbackRating i").on("click", function(){
        feedbackRating = $(this).attr("stars");
        _.each(_.range(0, feedbackRating), function (i) {
          i++;
          $("#feedbackRating i[stars='" + i + "']").removeClass("fa-star-o").addClass("fa-star");
        });
        _.each(_.range(parseInt(feedbackRating)+1, 6), function (i) {
          $("#feedbackRating i[stars='" + i + "']").removeClass("fa-star").addClass("fa-star-o");
        });
      });
    });
  }
});

Template.ListMyInvoices.helpers({
   handymanName: function () {
     return (this.handyman_profile != null && this.handyman_profile.fullName ? this.handyman_profile.fullName : "Some handyman");
   },
   date: function () {
     return moment(this.dateServicing).format("dddd, MMMM Do YYYY HH:mm");
   },
   destinationAddress: function () {
     return this.destinationAddress;
   },
   serviceName: function () {
     return this.service.name;
   },
   checkInvoice: function(){
     return !_.isUndefined(this.invoice);
   },
   invoiceServiceCost: function() {
     return !_.isUndefined(this.invoice) && !_.isUndefined(this.invoice.serviceCost) ? this.invoice.serviceCost : false;
   },
   invoiceTransportCost: function() {
     return !_.isUndefined(this.invoice) && !_.isUndefined(this.invoice.transportCost) ? this.invoice.transportCost : false;
   },
   invoiceExtrasCost: function() {
     return !_.isUndefined(this.invoice) && !_.isUndefined(this.invoice.extrasCost) ? this.invoice.extrasCost : false;
   },
   invoiceTax: function() {
     return !_.isUndefined(this.invoice) && !_.isUndefined(this.invoice.tax) ? this.invoice.tax : false;
   },
   invoiceTotal: function() {
     return !_.isUndefined(this.invoice) && !_.isUndefined(this.invoice.total) ? this.invoice.total : false;
   },
   unpaid: function() {
     return !_.isUndefined(this.invoice) && this.invoice.status == "unpaid" ? true : false;
   },
   idInvoice: function() {
     return !_.isUndefined(this.invoice) ? this.invoice._id: "";
   }
});

Template.ListMyInvoices.events({
  'click #payInvoice': function (e) {
    var idinvoice = $(e.target).data("idinvoice");
    Meteor.call("payInvoice", idinvoice, function(error, result) {
      if(error) {
        myApp7.alert(error, "Ups, something happened");
        return;
      } else {
        showNotification("Paid");
      }
    });
  },
});

/*
 * {{> PDRender name='customTemplate' data=.}}
 * Will check existing web_customTemplate, ios_customTemplate, android_customTemplate on working device
 */
UI.registerHelper('PDRender', function () {
    var component;
    if ((Meteor.isClient && Platform.isWeb()) || Session.get('platformOverride') === 'web') {
        component = Template["web_"+this.name];
        if (!_.isUndefined(component)) {
          return Blaze._getTemplate("web_"+this.name, function () {
            return Template.instance();
          });
        } else {
          return Blaze._getTemplate(this.name, function () {
            return Template.instance();
          });
        }
    }
    if ((Platform.isIOS()) || Session.get('platformOverride') === 'ios') {
        component = Template["ios_"+this.name];
        if (!_.isUndefined(component)) {
          return Blaze._getTemplate("ios_"+this.name, function () {
            return Template.instance();
          });
        } else {
          return Blaze._getTemplate(this.name, function () {
            return Template.instance();
          });
        }
    }
    if ((Platform.isAndroid()) || Session.get('platformOverride') === 'android') {
        component = Template["android_"+this.name];
        if (!_.isUndefined(component)) {
          return Blaze._getTemplate("android_"+this.name, function () {
            return Template.instance();
          });
        } else {
          return Blaze._getTemplate(this.name, function () {
            return Template.instance();
          });
        }
    }
    return UI.Component;
});

UI.registerHelper("formTypeMethod", function(){
  if(_.isEmpty(this)) {
    return 'method'
  } else {
    return 'method-update';
  }
});

/**
 * @DEPRECATED

Session.setDefault('counter', 0);

Template.Home.helpers({
  counter: function () {
    return Session.get('counter');
  }
})

Template.Home.events({
  'click button': function () {
    // increment the counter when button is clicked
    Session.set('counter', Session.get('counter') + 1);
  }
});
 */
 Template.LeftMenu.helpers({
     active: function(route) {
         return Router.current().route.getName() == route ? 'list-group-item-info' : '';
     }
 });

Template.SignIn.events({
    'mouseDown #login-buttons-google': function () {
        $("#login-buttons-google").addClass('active-state');
    },
    'mouseUp #login-buttons-google': function () {
        $("#login-buttons-google").removeClass('active-state');
    },
    'mouseDown #login-buttons-twitter': function () {
        $("#login-buttons-twitter").addClass('active-state');
    },
    'mouseUp #login-buttons-twitter': function () {
        $("#login-buttons-twitter").removeClass('active-state');
    },
    'mouseDown #login-buttons-facebook': function () {
        $("#login-buttons-facebook").addClass('active-state');
    },
    'mouseUp #login-buttons-facebook': function () {
        $("#login-buttons-facebook").removeClass('active-state');
    },
    'mouseDown #login-buttons-password': function () {
        $("#login-buttons-password").addClass('active-state');
    },
    'mouseUp #login-buttons-password': function () {
        $("#login-buttons-password").removeClass('active-state');
    }
});

Template.extendOnPlatforms(['android','ios','web'], 'SignIn');

Template.ClientProfileForm.helpers({
    ClientProfileFormSchema: function() {
        return schemas.client_profile;
    },
});

Template.ClientProfileForm.events({
    'click #reset': function () {
        Router.go("/");
    },
    'click #submit': function () {
        if(!AutoForm.validateForm("ClientProfileForm")) {
          return;
        }
        var doc = AutoForm.getFormValues("ClientProfileForm");
        doc._id = $("#docid").val();

        if(!_.isEmpty(doc.insertDoc.cardnumber) &&
           !_.isEmpty(doc.insertDoc.cvc) &&
           !_.isEmpty(doc.insertDoc.expired)
        ) {
          var expired = new String(doc.insertDoc.expired).split('/');
          var cardnumber = new String(doc.insertDoc.cardnumber).replace(" ", "");
          Stripe.card.createToken({
              number: doc.insertDoc.cardnumber,
              cvc: doc.insertDoc.cvc,
              exp_month: expired[0],
              exp_year: expired[1],
          }, function(status, response) {

            /**
            * DON'T SEND ANY CARD DATA TO SERVER!
            */
            delete(doc.insertDoc.cardnumber);
            delete(doc.insertDoc.cvc);
            delete(doc.insertDoc.expired);
            delete(doc.updateDoc.$set.cardnumber);
            delete(doc.updateDoc.$set.cvc);
            delete(doc.updateDoc.$set.expired);
            /**/

            if (response.error) { // Problem!
              var context = schemas.client_profile.namedContext("ClientProfileForm");

              switch (response.error.code) {
                case "invalid_number":
                  context.addInvalidKeys([{name: "cardnumber", type: response.error.code}]);
                  break;
                case "invalid_expiry_month":
                  context.addInvalidKeys([{name: "expired", type: response.error.code}]);
                  break;
                case "invalid_expiry_year":
                  context.addInvalidKeys([{name: "expired", type: response.error.code}]);
                  break;
                case "invalid_cvc":
                  context.addInvalidKeys([{name: "cvc", type: response.error.code}]);
                  break;
                case "incorrect_number":
                  context.addInvalidKeys([{name: "cardnumber", type: response.error.code}]);
                  break;
                case "expired_card":
                  context.addInvalidKeys([{name: "cardnumber", type: response.error.code}]);
                  break;
                case "incorrect_cvc":
                  context.addInvalidKeys([{name: "cvc", type: response.error.code}]);
                  break;
                case "card_declined":
                  context.addInvalidKeys([{name: "cardnumber", type: response.error.code}]);
                  break;
                case "missing":
                  context.addInvalidKeys([{name: "cardnumber", type: response.error.code}]);
                  break;
                case "processing_error":
                  context.addInvalidKeys([{name: "cardnumber", type: response.error.code}]);
                  break;
              }

              return;
            } else {
              doc.insertDoc.stripeToken = response.id;
              doc.updateDoc.$set.stripeToken = response.id;
              Meteor.call("sendClientProfile", doc, function(error, result) {
                if(error) {
                  myApp7.alert(error, "Ups, something happened");
                  return;
                }

                //if(result) {
                  showNotification("Saved");
                //}
              });
            }
          });
        } else {

          /**
          * DON'T SEND ANY CARD DATA TO SERVER! EVEN PARTLY
          */
          delete(doc.insertDoc.cardnumber);
          delete(doc.insertDoc.cvc);
          delete(doc.insertDoc.expired);
          delete(doc.updateDoc.$set.cardnumber);
          delete(doc.updateDoc.$set.cvc);
          delete(doc.updateDoc.$set.expired);
          /**/

          Meteor.call("sendClientProfile", doc, function(error, result) {
            if(error) {
              myApp7.alert(error, "Ups, something happened");
              return;
            }

            if(result) {
              showNotification("Saved");
            }
          });
        }
    }
    //save by method
});

var onRenderedClientMap = function(){
    myApp7 = new Framework7({
        router: false,
        //swipePanel: 'left',
        swipeBackPage: true,
        animatePages: true
    });

    Tracker.autorun(function () {
      var _services_subscribe = Meteor.subscribe('services');
      if (_services_subscribe.ready()) {
        var instances = services.find({}, {fields: {name: 1, _id: 1}}).fetch();
        var pickerService = myApp7.picker({
            input: '#picker-service',
            rotateEffect: true,
            cols: [
                {
                 textAlign: 'center',
                 values: _.pluck(instances,'name')
                }
            ],
            onChange: function(p, value, displayValue) {
              globalDict.set("requestService", value);
            },
            toolbarCloseText: "Done",
            onOpen: function (picker) {
              picker.container.find("#cancel_picker_service").on("click", function() {
                $("#picker-service").val("");
                picker.close();
                return false;
              });
            },
            toolbarTemplate: '<div class="toolbar">' +
                                '<div class="toolbar-inner">' +
                                  '<div class="left"><a href="#" class="link close-picker" id="cancel_picker_service">Cancel</a></div>' +
                                  '<div class="right">' +
                                    '<a href="#" class="link close-picker">{{closeText}}</a>' +
                                  '</div>' +
                                '</div>' +
                              '</div>'
        });

      }
    });

    var today = new Date();
    var nameMonths = ('January February March April May June July August September October November December').split(' ');
    var curr_minutes = (Math.ceil(today.getMinutes() / 10))*10 == 60 ? 50 : (Math.ceil(today.getMinutes() / 10))*10 ;
    var curr_hours = (today.getHours() == 23 ? 23 : today.getHours()+1);
    var pickerDate = myApp7.picker({
        input: '#picker-date',
        rotateEffect: true,

        value: [today.getMonth(), today.getDate(), (curr_hours < 10 ? '0' + curr_hours : curr_hours), (curr_minutes < 10 ? '0' + curr_minutes : curr_minutes) ],

        onChange: function (picker, values, displayValues) {
            var daysInMonth = new Date(picker.value[2], picker.value[0]*1 + 1, 0).getDate();
            if (values[1] > daysInMonth) {
                picker.cols[1].setValue(daysInMonth);
            }
            globalDict.set("requestDate", values);
        },
        toolbarCloseText: "Done",
        onOpen: function (picker) {
          picker.container.find("#cancel_picker_date").on("click", function() {
            $("#picker-date").val("");
            picker.close();
            return false;
          });
        },
        toolbarTemplate: '<div class="toolbar">' +
                            '<div class="toolbar-inner">' +
                              '<div class="left"><a href="#" class="link close-picker" id="cancel_picker_date">Cancel</a></div>' +
                              '<div class="right">' +
                                '<a href="#" class="link close-picker">{{closeText}}</a>' +
                              '</div>' +
                            '</div>' +
                          '</div>',
        formatValue: function (p, values, displayValues) {
            return (_.isUndefined(displayValues[0]) ? nameMonths[values[0]] : displayValues[0]) + ' ' + values[1] + ', ' + values[2] + ':' + values[3];
        },

        cols: [
            // Months
            {
                values: ('0 1 2 3 4 5 6 7 8 9 10 11').split(' '),
                displayValues: nameMonths,
                textAlign: 'left'
            },
            // Days
            {
                values: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
            },
            // Space divider
            {
                divider: true,
                content: '  '
            },
            // Hours
            {
                values: (function () {
                    var arr = [];
                    for (var i = 0; i <= 23; i++) { arr.push(i < 10 ? '0' + i : i); }
                    return arr;
                })(),
            },
            // Divider
            {
                divider: true,
                content: ':'
            },
            // Minutes
            {
                values: (function () {
                    var arr = [];
                    for (var i = 0; i <= 59; i +=10) { arr.push(i < 10 ? '0' + i : i); }
                    return arr;
                })(),
            }
        ]
    });

    myApp7.closePanel();

    AdaptiveMaps.init({
       //'sensor': true, //optional
       'key': (Meteor.settings.public && Meteor.settings.public.google_maps ? Meteor.settings.public.google_maps: "")
       //'language': 'es' //optional
    }, function () {
       /*
       if(!location) {
         myApp7.alert("Our App could not define your coordinates, please check your settings of a restrictions for a geolocation.", "Ups, something happened");
       }
       */
       var currentPositionClient =  new AdaptiveMaps.LatLng(40.735877, -73.990394);
       var _currentUserProfile = client_profile.find({"id_Client": Meteor.userId()}).fetch();

       if(!_.isEmpty(_currentUserProfile)) {
         currentPositionClient =  new AdaptiveMaps.LatLng(_currentUserProfile[0].currentPosition.lat, _currentUserProfile[0].currentPosition.lng);
       }
       var center_adaptiveMap = currentPositionClient;
       var mapOptions = {
           mapTypeId: 'ROADMAP',
           camera: {
               latLng: center_adaptiveMap,
               zoom: 12
           }
       }

       adaptiveMap = new AdaptiveMaps.Map(document.getElementById('map'), mapOptions);

       adaptiveMap.addEventListener("center_changed", function() {
         var center = adaptiveMap.getCenter();
         if(this.timer) {
           clearTimeout(this.timer);
         }
         this.timer = setTimeout(function() {
           if(this.sub) {
             this.sub.stop();
           }
           Meteor.call("updateClientLocation", {"lat": center.lat(), "lng": center.lng()});
           this.sub = Meteor.subscribe('handymen', {"lat": center.lat(), "lng": center.lng()});
           //console.log({"lat": center.lat(), "lng": center.lng(), "sub": this.sub});
         }, 500);
       });

       adaptiveMap.addLiveMarkers({
          //@TODO
          // we must be aware that current location present, because it happens, that it doesn't exist in db yet
    			cursor: handyman_profile.find(),
     			transform: function (profile) {
            //console.debug(profile);
            //value: [today.getMonth(), today.getDate(), (today.getHours() == 23 ? 23 : today.getHours()+1), (curr_minutes < 10 ? '0' + curr_minutes : curr_minutes) ],
            var time = globalDict.get("requestDate") != null ? parseInt(globalDict.get("requestDate")[2]) * 60 +  parseInt(globalDict.get("requestDate")[3]) : new Date().getHours() * 60 + new Date().getMinutes();
            var icon = '/icon/handyman.png';
            if(!_.isUndefined(profile.availability) &&
               !_.isUndefined(profile.availability.from) &&
               !_.isUndefined(profile.availability.to) &&
               profile.availability.from < time &&  profile.availability.to > time) {
              icon = '/icon/handymanAvailable.png';
            }
     				return {
              _id: profile._id,
     					title: profile.fullName,
     					position: new AdaptiveMaps.LatLng(profile.currentPosition.lat, profile.currentPosition.lng),
     					profile: profile,
              icon: icon,
              // This marker is 20 pixels wide by 32 pixels high.
              size: new google.maps.Size(32, 32),
              // The origin for this image is (0, 0).
              origin: new google.maps.Point(0, 0),
              // The anchor for this image is the base of the flagpole at (0, 32).
              anchor: new google.maps.Point(0, 32),
              infoWindowAnchor: [0,0],
     				}
     			},
     			onClick: function (event) {
            var _profile = this.profile;
            var rating   = "";
            _.each(_.range(0, 5), function (i) {
              i++;
              if(i <= _profile.rating) {
                rating += '<i class="fa fa-2 fa-star"></i>';
              }else {
                rating += '<i class="fa fa-2 fa-star-o"></i>';
              }
            });

            var profile_buttons = [
              {
                text: 'Close',
                bold: true,
                close: true,
              }
            ];

            var existing_request_handyman = service_request.findOne({"id_Handyman" : _profile.id_Handyman, "status": {"$not": {"$in": ["paid", "closed"]}}});

            if(!_.isEmpty(existing_request_handyman)) {
              profile_buttons.push(
                {
                  text: 'Go to chat',
                  bold: true,
                  onClick: function() {
                    Router.go(Router.routes['Chat'].path({"_id": _profile.id_Handyman}));
                    return false;
                  }
                }
              );
            }

            profile_buttons.push(
              {
                text: 'Pick',
                bold: true,
                close: true,
                onClick: function() {
                  globalDict.set("id_HandymanRequest", _profile.id_Handyman);
                  return false;
                }
              }
            );

            myApp7.modal({
              title:  _.isUndefined(_profile.fullName) ? 'Profile of handyman' : _profile.fullName,
              text: '',
              afterText: '<div class="list-block">'+
                            '<ul>'+
                              '<li class="align-top">'+
                                  '<div class="item-title full-label">Mobile phone</div>'+
                                  '<div class="item-input">'+
                                    '<input type="text" class="mobile-phone" readonly value="' + (_.isUndefined(_profile.phone) ? 'You must use chat for contact' : _profile.phone) + '"/>'+
                                  '</div>'+
                              '</li>'+
                              '<li class="align-top">'+
                                  '<div class="item-title full-label">Rating</div>'+
                                  '<div class="item-input">'+
                                    '<p>' + rating + '</p>' +
                                  '</div>'+
                              '</li>'+
                              '<li class="align-top">'+
                                  '<div class="item-title full-label">Feedbacks</div>'+
                                  '<div class="item-input">'+
                                      '<p>' + ((_.isUndefined(_profile.countFeedbacks) || _profile.countFeedbacks == 0) ? 'No yet. Be first!' : 'Check <a href = ' + Router.routes['HandymanFeedbacks'].path({"_id": _profile.id_Handyman}) + '>' + _profile.countFeedbacks + '</a>') + '</p>' +
                                  '</div>'+
                              '</li>'+
                            '</ul>'+
                          '</div>',
              buttons: profile_buttons
            });
            //end of modal
     			}
     	 });

       Tracker.autorun(function () {
         var locationRequest = globalDict.get("locationRequest");
         if(!locationRequest) {
           return;
         }
         var service_id = globalDict.get("requestService");
         var id_HandymanRequest = globalDict.get("id_HandymanRequest");
         var transportCost = false;
         if(service_id) {
           var service = services.findOne({"name": service_id[0]});
           if(!service) {
             return;
           }
           transportCost = service.transportCost;
         }
         handyman_profile.find().forEach(function(profile) {
           if(_.isUndefined(adaptiveMap._markers[profile._id])) {
             return;
           }
           handyman_profile._collection.update({_id: profile._id}, {$set: {"distance" : _distance_content}});

           var distance = geolib.getDistance(
               {latitude: profile.currentPosition.lat, longitude: profile.currentPosition.lng},
               {latitude: locationRequest.lat, longitude: locationRequest.lng}
           );

           var _distance_content = Math.ceil(distance/1000);
           var content = "";
           if(!_.isUndefined(id_HandymanRequest) && id_HandymanRequest == profile.id_Handyman) {
             content += "<i class=\"fa fa-check fa-green fa-1-5\"></i>";
           }
           //_distance_content +
           content += "ETA: ~" + (Math.ceil(_distance_content*0.66)) + " mins"; // average speed 40 km per hour = 0,66 km per min
           if(transportCost) {
             content += "</br> TP Fee: " + _distance_content * transportCost;
           }

           if(_.isUndefined(adaptiveMap._markers[profile._id]._infoWindows)) {
             adaptiveMap._markers[profile._id]._infoWindows = new google.maps.InfoWindow({
                 content:  content,
                 disableAutoPan: true
             });

             adaptiveMap._markers[profile._id]._infoWindows.open(adaptiveMap.originalMap, adaptiveMap._markers[profile._id]);
           } else {
             adaptiveMap._markers[profile._id]._infoWindows.setContent(content);
             if(adaptiveMap._markers[profile._id]._infoWindows.map == null ||
                _.isUndefined(adaptiveMap._markers[profile._id]._infoWindows.map)) {

               adaptiveMap._markers[profile._id]._infoWindows.open(adaptiveMap.originalMap, adaptiveMap._markers[profile._id]);
             }
           }
         });

       });

       Tracker.autorun(function(){
           if(globalDict.get("requestDate") != null) {
             time = parseInt(globalDict.get("requestDate")[2]) * 60 +  parseInt(globalDict.get("requestDate")[3]);
             handyman_profile.find().forEach(function(profile) {
               if(_.isUndefined(adaptiveMap._markers[profile._id])) {
                 return;
               }
               var icon = '/icon/handyman.png';
               if(!_.isUndefined(profile.availability) &&
                  !_.isUndefined(profile.availability.from) &&
                  !_.isUndefined(profile.availability.to) &&
                  profile.availability.from < time &&  profile.availability.to > time) {
                 icon = '/icon/handymanAvailable.png';
               }
               adaptiveMap._markers[profile._id].setIcon(icon);
             });
           }
       });

    });

    //Init typeWatch plugin for address searching
    $("#getAddressGeocoder").typeWatch( {
        callback: function (value) {
          $("#getAddressGeocoder").trigger('typeWatch', this);
        },
        wait: 750,
        captureLength: 2
    } );

};

Template.ClientMap.onRendered(onRenderedClientMap);

Template.ClientMapBlock.onCreated(function() {
  this.subscribe("profile");
});
Template.ClientMapBlockBody.onRendered(onRenderedClientMap);

Template.Support.helpers({
  contactFormSchema: function() {
    return schemas.contact;
  }
});

Template.Support.events({
    'click #reset': function () {
        Router.go("/");
    },
    'click #submit': function () {
        if(!AutoForm.validateForm("supportForm")) {
          return false;
        }
        var doc = AutoForm.getFormValues("supportForm");

        Meteor.call("sendSupport", doc, function(error, result) {
          if(error) {
            myApp7.alert(error, "Ups, something happened");
            return;
          }

          if(result) {
            showNotification("Saved");
          }
        });
    }
    //save by method
});

Template.RightMenu.helpers({
  Title: function() {
    if(Router.current().route.options.title) {
      return Router.current().route.options.title;
    } else {
        return false;
    }
  },
  showTitle: function() {
    return _.indexOf(["Chat", "ClientMap"], Router.current().route.getName() ) === -1;
  },
  toolbarMessage: function() {
    return Router.current().route.getName() === "Chat";
  }
});

for(var property in Template){
  // check if the property is actually a blaze template
  if(Blaze.isTemplate(Template[property])){
    var template=Template[property];
    // assign the template an onRendered callback who simply prints the view name
    template.onRendered(function(){
      myApp7 = new Framework7({
          router: false,
          //swipePanel: 'left',
          swipeBackPage: true,
          animatePages: true
      });
      myApp7.closePanel();
    });
  }
}

Template.extendOnPlatforms(['android','ios','web'], 'LeftMenu');
Template.extendOnPlatforms(['android','ios','web'], 'ClientProfileForm');
Template.extendOnPlatforms(['android','ios','web'], 'RightMenu');
Template.extendOnPlatforms(['android','ios','web'], 'Support');
Template.extendOnPlatforms(['android','ios','web'], 'ListMyRequests');
Template.extendOnPlatforms(['android','ios','web'], 'ListMyInvoices');
Template.extendOnPlatforms(['android','ios','web'], 'ListMyChats');
Template.extendOnPlatforms(['android','ios','web'], 'Chat');

Template.web_Chat.helpers({
   puredate: function () {
     return moment(this.date).format("dddd, MMMM Do YYYY");
   },
   puretime: function () {
     return moment(this.date).format("HH:mm");
   },
   myMessage: function () {
     return (Meteor.userId() == this.from ? "right" : "left");
   },
   author: function () {
     var name = "";
     if(Meteor.userId() == this.from) {
       name = "It is my";
     } else {
       var _handyman_profile = handyman_profile.findOne({"id_Handyman": this.from});
       if(_handyman_profile != null && _handyman_profile.fullName) {
         name = _handyman_profile.fullName
       } else {
         name = "Handyman";
       }
     }
     return name;
   }
});

Template.web_Chat.events({
  "click #loadHistory": function() {
    $(".page-content").trigger("refresh");
  }
});

Template.web_Chat.onRendered(function () {

  this.find('.chat')._uihooks = {
    insertElement: function (node, next) {
      $(node).insertBefore(next);

      Deps.afterFlush(function() {
        // call width to force the browser to draw it
        //$(node).width();
        var height = 0;
        if(!Session.get("loadingHistory")) {
          height = $(".chat").outerHeight();
        }
        $(".messages").stop().animate({scrollTop: height}, '500', 'swing');
      });
    }
  }
  var height = $(".chat").outerHeight();

  Push.addListener('message', function(notification) {
    console.log(notification);
  });

  $(".messages").stop().animate({scrollTop: height}, '500', 'swing');
});
