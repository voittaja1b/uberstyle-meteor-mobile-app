Template.ApplicationLayoutDynamic.helpers({
  showLeftMenu: function() {
    if(!Meteor.userId()) {
        return false;
    } else {
        return true;
    }
  },
  showLeftMenuIcon: function() {
    if(!Meteor.userId() || ((Meteor.isClient && Platform.isWeb() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'web')) {
        return false;
    } else {
        return true;
    }
  },
  showRightMenu: function() {
    if(!Meteor.userId()) {
        return false;
    } else {
        return true;
    }
  },
  showRightMenuIcon: function() {
    if(!Meteor.userId() || ((Meteor.isClient && Platform.isWeb() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'web')) {
        return false;
    } else {
        return true;
    }
  },
  showSubNavBar: function() {
    return Router.current().route.getName() === "ClientMap";
  },
  toolbarMap: function() {
    return Router.current().route.getName() === "ClientMap";
  },
  toolbarMessage: function() {
    return Router.current().route.getName() === "Chat";
  },
  historyBack: function() {
    if(!Meteor.userId() || ((Meteor.isClient && Platform.isWeb() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'web')) {
        return false;
    } else {
        return Session.get("previousLocationPath") != null ? true : false;
    }
  }
});

var attachNotification = function() {
  notification_history.find().observe({
    added: function (doc) {
      var link = "";
      if((Router.current().route.getName() == doc.path && _.isUndefined(doc.pathId)) ||
         (Router.current().route.getName() == doc.path && !_.isUndefined(doc.pathId) && Router.current().params._id == doc.pathId)) {
           Meteor.call("deleteNotification", doc._id);
        return;
      } else {
        _.map(Router.routes, function(func){
          if(func.getName() == doc.path) {
            link = func.options.path;
          }
        });
        if(!_.isUndefined(doc.pathId)) {
          link = new String(link).replace(":_id", doc.pathId);
        }
      }
      var message = "<a href='" + link + "'>" + (!_.isUndefined(doc.from) ? "<b>" + doc.from + ":</b>" : "") + " " + doc.message  + "</a>";
      showNotification(doc._id, message);
    },
    changed: function (doc) {

    },
    removed: function (doc) {
      if (Meteor.isCordova) {

      } else {
        if(Platform.isWeb) {
          bootstrap_alert.close(doc._id);
        }else {

        }
      }
    }
  });
};

//Init Framework7
Template.ApplicationLayoutDynamic.onRendered(function() {

    // Initialize app on server and on client
    myApp7 = new Framework7({
        router: false,
        //swipePanel: 'left',
        swipeBackPage: true,
        animatePages: true
    });

    // If we need to use custom DOM library, let's save it to $$ variable:
    $$ = Dom7;

    mainView = myApp7.addView('.view-main', {
        onSwipeBackBeforeChange: function(callbackData) {
            history.back();
        }
    });

    this.find('.pages')._uihooks = {
        insertElement: function(node, next) {
            mainView.router.loadContent(node);
        },
        removeElement: function(node) {
            return false;
        }
    };

    //prevent show panels
    $("body").removeClass("with-panel-left-reveal").removeClass("with-panel-right-reveal");

    //Notification
    attachNotification();
});

Template.web_ApplicationLayoutDynamic.onRendered(function() {
  var platform = 'platform-web';
  if ((Meteor.isCordova && Platform.isIOS() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'ios') {
    platform = 'platform-ios';
  }
  if ((Meteor.isCordova && Platform.isAndroid() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'android') {
    platform = 'platform-android';
  }
  $("body").addClass(platform);

  attachNotification();
});

Template.AccountsLayout.onRendered(function() {

    // Initialize app on server and on client
    myApp7 = new Framework7({
        router: false,
        //swipePanel: 'left',
        swipeBackPage: true,
        animatePages: true
    });

    // If we need to use custom DOM library, let's save it to $$ variable:
    $$ = Dom7;

    mainView = myApp7.addView('.view-main', {
        onSwipeBackBeforeChange: function(callbackData) {
            history.back();
        }
    });

    this.find('.pages')._uihooks = {
        insertElement: function(node, next) {
            mainView.router.loadContent(node);
        },
        removeElement: function(node) {
            return false;
        }
    };

    //prevent show panels
    $("body").removeClass("with-panel-left-reveal").removeClass("with-panel-right-reveal");
});

Template.ApplicationLayoutDynamic.events({
  "click #historyBack": function() {
    Router.go(Session.get("previousLocationPath"));
    return false;
  },
  "click #getMyLocation": function(event, template) {
    var marker = {
      _id: "i",
      transform: function(marker) {
        return marker;
      },
      onClick: function (event) {
        console.log(event);
      },
      icon: '/icon/user_location.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(32, 32),
      // The origin for this image is (0, 0).
      origin: new google.maps.Point(0, 0),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(0, 32),
      infoWindowAnchor: [0,0]
    };

    //Here we just watch for reactive data and change location for marker and center for Map
    Tracker.autorun(function () {
      var location = Geolocation.currentLocation();
      var error = Geolocation.error();
      if(error) {
        switch (error.code) {
          case error.PERMISSION_DENIED:
              myApp7.alert("Device denied the request for Geolocation", "Ups, something happened");
              adaptiveMap.removeMarker(marker);
              break;
          case error.POSITION_UNAVAILABLE:
              myApp7.alert("Location information is unavailable.", "Ups, something happened");
              adaptiveMap.removeMarker(marker);
              break;
          case error.TIMEOUT:
              myApp7.alert("The request to get device location timed out.", "Ups, something happened");
              adaptiveMap.removeMarker(marker);
              break;
          case error.UNKNOWN_ERROR:
              myApp7.alert("An unknown error occurred.", "Ups, something happened");
              adaptiveMap.removeMarker(marker);
              break;
          default:
            break;
        }
      }

      if(!location) {
        return;
      }

      adaptiveMap.setCenter(new AdaptiveMaps.LatLng(location.coords.latitude, location.coords.longitude));
      marker.position = new AdaptiveMaps.LatLng(location.coords.latitude, location.coords.longitude);

      if(adaptiveMap._markers["i"]) {
        adaptiveMap.changeMarker(marker);
      } else {
        adaptiveMap.addMarker(marker);
      }
    });

    //restrict of redirection
    return false;
  },
  "click #setLocationMarker": function(event, template) {
    var location = adaptiveMap.originalMap.getCenter();

    var marker = {
      _id: "pin",
      transform: function(marker) {
        globalDict.set("locationRequest", {lat:location.lat(), lng:location.lng()});
        return marker;
      },
      onClick: function (event) {
        console.log(event);
      },
      onDragEnd: function(event) {
        globalDict.set("locationRequest", {lat:event.latLng.lat(), lng:event.latLng.lng()});
      },
      position: location,
      icon: '/icon/pin.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(32, 32),
      // The origin for this image is (0, 0).
      origin: new google.maps.Point(0, 0),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(0, 32),
      infoWindowAnchor: [0,0],
      draggable: true
    }

    adaptiveMap.addMarker(marker);
  },
  "typeWatch #getAddressGeocoder": function(event, template) {
      var address = template.find("#getAddressGeocoder").value;
      // Just we don't allow address less then 3 symbols
      if(address.length < 4) {
        return true;
      }
      Meteor.call("locationByAddress", address, function(error, result){
        if(error) {
          myApp7.alert("Sorry, we cannot determine a location by this address.", "Ups, something happened");
          //@TODO remove this
          if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
              console.log("error", error);
          }
        }
        if(result && result.length > 0) {
          //result[0].formattedAddress:"Khod, Rajasthan 306119, India"
          globalDict.set("locationCenter", {"lat": result[0].latitude, "lng": result[0].longitude});
          adaptiveMap.setCenter(new AdaptiveMaps.LatLng(result[0].latitude, result[0].longitude));

          //@TODO carry out in other extended Geocoder api file
          var zoom = 12;
          var tabZoom =  {
              streetNumber: 20,
              streetName: 15,
              zipcode: 13,
              city: 10,
              country: 5
          };
          for(var i in tabZoom) {
            if (typeof(result[0][i]) != "undefined"){
              zoom = tabZoom[i];
              break;
            }
          }
          adaptiveMap.setZoom(zoom);
        }
      });
  },
  "click #createRequest": function(event, template) {
    var invalid = false;
    var service = globalDict.get("requestService");
    var destinationPoint = globalDict.get("locationRequest");
    var id_HandymanRequest = globalDict.get("id_HandymanRequest");
    var handymanName = "Some handyman";

    if(_.isUndefined(client_profile.findOne({"id_Client": Meteor.userId()}).certified) ||
       client_profile.findOne({"id_Client": Meteor.userId()}).certified == false
      ) {
      myApp7.alert("You have forgot fill billing information or your card is expired. Please enter new or updated billing information on profile page.", "Ups, something happened");
      invalid = true;
      return;
    }
    if(!service) {
      myApp7.alert("You have forgot choose a service in a bottom toolbar.", "Ups, something happened");
      invalid = true;
    }
    if(!destinationPoint) {
      myApp7.alert("You have forgot choose a location on the map. Click on a marker icon in a right up corner and drag on a right place.", "Ups, something happened");
      invalid = true;
    }
    if(!invalid) {
      if(id_HandymanRequest != null) {
        var _handyman = handyman_profile.findOne({"id_Handyman": id_HandymanRequest});
        if(!_.isUndefined(_handyman)) {
          var time = parseInt(globalDict.get("requestDate")[2]) * 60 +  parseInt(globalDict.get("requestDate")[3]);
          var available = false;
          if(!_.isUndefined(_handyman.availability) &&
             !_.isUndefined(_handyman.availability.from) &&
             !_.isUndefined(_handyman.availability.to) &&
             _handyman.availability.from < time &&  _handyman.availability.to > time) {
            available = true;
          }
          if(!available) {
            myApp7.alert("Sorry, selected handyman unavailable in this time.", "Ups, something happened");
            return;
          }
        } else {
          myApp7.alert("Pick handyman.", "Ups, something happened");
          return;
        }
        handymanName = (!_.isUndefined(handyman_profile.findOne({"id_Handyman": id_HandymanRequest})) ? handyman_profile.findOne({"id_Handyman": id_HandymanRequest}).fullName : "Some handyman");
      } else {
        //Search nearest handyman
        var spots = {};
        var _profiles = handyman_profile.find().fetch();
        for (var i in _profiles) {
          spots[_profiles[i].id_Handyman] = {"latitude": _profiles[i].currentPosition.lat, "longitude": _profiles[i].currentPosition.lng};
        }
        var nearest_id_Handyman = geolib.findNearest({"latitude": destinationPoint.lat, "longitude": destinationPoint.lng}, spots);

        if(nearest_id_Handyman == null) {
          myApp7.alert("Sorry, we cannot find any handyman nearby this location.", "Ups, something happened");
          return;
        }
        globalDict.set("id_HandymanRequest", nearest_id_Handyman.key);
        handymanName = (!_.isUndefined(handyman_profile.findOne({"id_Handyman": nearest_id_Handyman.key})) ? handyman_profile.findOne({"id_Handyman": nearest_id_Handyman.key}).fullName : "Some handyman");
      }

      Meteor.call("addressByLocation", destinationPoint, function(error, result){
        if(error) {
          console.log(error);
          myApp7.alert("Sorry, we cannot determine an address by this location.", "Ups, something happened");
          return;
        }
        if(result && result.length > 0) {
          var realAddressRequest = result[0].formattedAddress;
          myApp7.modal({
            title:  'Review of the request',
            text: 'Please make sure that all is right below',
            afterText: '<div class="list-block">'+
                          '<ul>'+
                            '<li class="align-top">'+
                                '<div class="item-title full-label">Service</div>'+
                                '<div class="item-input">'+
                                  '<input type="text" id="serviceRequest" readonly value="' + service[0] + '"/>'+
                                '</div>'+
                            '</li>'+
                            '<li class="align-top">'+
                                '<div class="item-title full-label">Сhosen location</div>'+
                                '<div class="item-input">'+
                                  '<input type="text" id="destinationAddress" readonly value="' + realAddressRequest + '"/>'+
                                '</div>'+
                            '</li>'+
                            '<li class="align-top">'+
                                '<div class="item-title full-label">Executor</div>'+
                                '<div class="item-input">'+
                                  '<input type="text" id="id_Handyman" readonly value="' + handymanName + '"/>'+
                                '</div>'+
                            '</li>'+
                            '<li class="align-top">'+
                                '<div class="item-title full-label">Comment</div>'+
                                '<div class="item-input">'+
                                  '<textarea id="commentRequest" placeholder="Describe your request here. Than you will done description fuller, the faster handyman will help you."></textarea>'+
                                '</div>'+
                            '</li>'+
                          '</ul>'+
                        '</div>',
            buttons: [
              {
                text: 'Cancel it',
                bold: true,
                close: true,
              },
              {
                text: 'Place it!',
                bold: true,
                onClick: function() {
                  var id_Service = services.findOne({"name": service[0]}, {fields: {_id: 1}});
                  var request = {
                    id_Service: id_Service._id,
                    destinationAddress: realAddressRequest,
                    destinationPoint: destinationPoint,
                    comment : $("#commentRequest").val(),
                    dateServicing: new Date(new Date().getFullYear(), globalDict.get("requestDate")[0], globalDict.get("requestDate")[1] , globalDict.get("requestDate")[2], globalDict.get("requestDate")[3]),
                    id_Handyman: globalDict.get("id_HandymanRequest")
                  };
                  Meteor.call("placeRequest", request, function(error, result){
                    if(error){
                      console.log("error", error);
                      myApp7.alert(error, "Ups, something happened");
                    }
                    if(result){
                      myApp7.alert('The request have been placed.', 'Congratulation!');
                      if(adaptiveMap._markers["pin"]) {
                        adaptiveMap.removeMarker(adaptiveMap._markers["pin"]);
                        globalDict.set("requestService", null);
                        globalDict.set("id_HandymanRequest", null);
                        globalDict.set("locationRequest", null);
                        $('#picker-service').val('');
                        $('#picker-date').val('');
                      }
                    }
                  });
                }
              },
            ]
          });
        }
      });
    }
  },
  "click #sendMessage": function(event, t){
      var message = $("#bodyMessage").val();
      if(message) {
        $("#bodyMessage").val('');
        Meteor.call("sendMessageToChat", {"message" : message, "id_Chat": $("#chat_id").val()},function(error, result) {
          if(error) {
            console.log(error);
          }
          var height = $(".messages").outerHeight();
          $(".page-content").stop().animate({scrollTop: height}, '500', 'swing');
        });
      }
  },
  "refresh": function(event) {
    var message_count = $("#message_count").val();
    Tracker.autorun(function () {
      var _chat_subscribe = Meteor.subscribe('chatHistory', $("#chat_id").val(), parseInt(message_count) + 50);
      Session.set("loadingHistory", true);
      if (_chat_subscribe.ready()) {
        myApp7.pullToRefreshDone();
        setTimeout(function() {
          Session.set("loadingHistory", false);
        }, 500);
        $("#message_count").val(parseInt(message_count) + 50);
      }
    });
  }
});

Template.extendOnPlatforms(['android','ios','web'], 'ApplicationLayoutDynamic');

Template.web_ApplicationLayoutDynamic.helpers({
  showLeftMenu: function() {
    if(!Meteor.userId()) {
        return false;
    } else {
        return true;
    }
  },
  showLeftMenuIcon: function() {
    if(!Meteor.userId() || ((Meteor.isClient && Platform.isWeb() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'web')) {
        return false;
    } else {
        return true;
    }
  },
  showRightMenu: function() {
    if(!Meteor.userId()) {
        return false;
    } else {
        return true;
    }
  },
  showRightMenuIcon: function() {
    if(!Meteor.userId() || ((Meteor.isClient && Platform.isWeb() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'web')) {
        return false;
    } else {
        return true;
    }
  },
  Title: function() {
    return false;
  },
  showSubNavBar: function() {
    return true;
  },
  toolbarMap: function() {
    return true;
  },
  showTitle: function() {
    return _.indexOf(["Chat"], Router.current().route.getName() ) === -1;
  },
  historyBack: function() {
    return false;
  }
});
